import { Component } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { UserService } from '@app/services/user.service';

@Component({
  selector: 'app-signup',
  templateUrl: './signup.component.html',
  styleUrls: ['./signup.component.css'],
})
export class SignUpComponent {
  public form: FormGroup;
  public isPasswordVisible: boolean;
  public isErrorAlertVisible: boolean;
  public errorMessage?: string;

  constructor(
    private userService: UserService,
    private formBuilder: FormBuilder,
    private router: Router
  ) {
    this.form = this.formBuilder.group({
      email: ['', [Validators.required, Validators.email]],
      username: [
        '',
        [
          Validators.required,
          Validators.minLength(2),
          Validators.maxLength(30),
        ],
      ],
      password: ['', [Validators.required, Validators.minLength(8)]],
    });
    this.isPasswordVisible = false;
    this.isErrorAlertVisible = false;
    this.errorMessage = 'TODO: An unknown error occured';
  }

  public togglePasswordVisibility(): void {
    this.isPasswordVisible = !this.isPasswordVisible;
  }

  public onErrorAlertClose(): void {
    this.isErrorAlertVisible = false;
  }

  public signup(): void {
    const email = this.form.value.email;
    const username = this.form.value.username;
    const password = this.form.value.password;

    // reset error state
    this.isErrorAlertVisible = false;
    // signup request
    this.userService.signup(email, username, password).subscribe(
      // on success
      () => this.router.navigateByUrl('/'),
      // on error
      (error) => {
        this.errorMessage = error;
        this.isErrorAlertVisible = true;
      }
    );
  }
}
