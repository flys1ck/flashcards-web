import { Component } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { Token, UserService } from '@app/services/user.service';

@Component({
  selector: 'app-signin',
  templateUrl: './signin.component.html',
  styleUrls: ['./signin.component.css'],
})
export class SignInComponent {
  public form: FormGroup;
  public isPasswordVisible: boolean;
  public isErrorAlertVisible: boolean;
  public errorMessage?: string;

  constructor(
    private userService: UserService,
    private formBuilder: FormBuilder,
    private router: Router
  ) {
    this.form = this.formBuilder.group({
      usernameOrEmail: ['', [Validators.required]],
      password: ['', [Validators.required]],
    });
    this.isPasswordVisible = false;
    this.isErrorAlertVisible = false;
    this.errorMessage = 'TODO: An unknown error occured';
  }

  public togglePasswordVisibility(): void {
    this.isPasswordVisible = !this.isPasswordVisible;
  }

  public onErrorAlertClose(): void {
    this.isErrorAlertVisible = false;
  }

  public signin(): void {
    const usernameOrEmail = this.form.value.usernameOrEmail;
    const password = this.form.value.password;
    this.userService.signin(usernameOrEmail, password).subscribe(
      (res: Token) => {
        localStorage.setItem('jwt', res.token);
        this.router.navigateByUrl('/');
      },
      (error) => {
        this.errorMessage = error;
        this.isErrorAlertVisible = true;
      }
    );
  }
}
