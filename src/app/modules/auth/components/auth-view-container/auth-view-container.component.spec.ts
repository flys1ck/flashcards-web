import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AuthViewContainerComponent } from './auth-view-container.component';

describe('AuthViewContainerComponent', () => {
  let component: AuthViewContainerComponent;
  let fixture: ComponentFixture<AuthViewContainerComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AuthViewContainerComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AuthViewContainerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
