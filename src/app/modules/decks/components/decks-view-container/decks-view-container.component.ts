import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-decks-view-container',
  templateUrl: './decks-view-container.component.html',
  styleUrls: ['./decks-view-container.component.css']
})
export class DecksViewContainerComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

}
