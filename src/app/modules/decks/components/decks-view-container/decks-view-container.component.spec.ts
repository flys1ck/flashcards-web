import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DecksViewContainerComponent } from './decks-view-container.component';

describe('DecksViewContainerComponent', () => {
  let component: DecksViewContainerComponent;
  let fixture: ComponentFixture<DecksViewContainerComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DecksViewContainerComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DecksViewContainerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
