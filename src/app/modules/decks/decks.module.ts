import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DecksViewContainerComponent } from './components/decks-view-container/decks-view-container.component';
import { DeckCardComponent } from './components/deck-card/deck-card.component';
import { NewDeckComponent } from './components/new-deck/new-deck.component';
import { RouterModule } from '@angular/router';

@NgModule({
  declarations: [
    DecksViewContainerComponent,
    DeckCardComponent,
    NewDeckComponent,
  ],
  imports: [CommonModule, RouterModule],
})
export class DecksModule {}
