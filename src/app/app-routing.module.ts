import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AuthGuard } from './guards/auth.guard';
import { SignInComponent } from './modules/auth/components/signin/signin.component';
import { SignUpComponent } from './modules/auth/components/signup/signup.component';
import { DashboardComponent } from './modules/dashboard/components/dashboard/dashboard.component';
import { DecksViewContainerComponent } from './modules/decks/components/decks-view-container/decks-view-container.component';
import { NewDeckComponent } from './modules/decks/components/new-deck/new-deck.component';

const routes: Routes = [
  {
    path: 'signin',
    component: SignInComponent,
  },
  {
    path: 'signup',
    component: SignUpComponent,
  },
  {
    path: 'dashboard',
    component: DashboardComponent,
    canActivate: [AuthGuard],
  },
  {
    path: 'decks',
    component: DecksViewContainerComponent,
  },
  {
    path: 'decks/new',
    component: NewDeckComponent,
  },
  {
    path: '',
    redirectTo: 'dashboard',
    pathMatch: 'full',
  },
  {
    path: '**',
    component: DashboardComponent,
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}
