import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, throwError } from 'rxjs';
import { catchError, take } from 'rxjs/operators';

export interface Token {
  token: string;
}

export interface User {
  _id: string;
  username: string;
  email: string;
  password: string;
}

@Injectable({
  providedIn: 'root',
})
export class UserService {
  private userUrl = 'http://localhost:3000/users/';

  constructor(private http: HttpClient) {}

  handleError(error: HttpErrorResponse): Observable<Error> {
    if (error.error instanceof ErrorEvent) {
      // TODO: client side error
      console.log(error.error);
    } else {
      // TODO: server side error
      console.log(error.error);
    }

    return throwError('An error happened');
  }

  getUsers(): Observable<User[]> {
    return this.http.get<User[]>(this.userUrl).pipe(take(1));
  }

  signup(email: string, username: string, password: string): Observable<any> {
    return this.http
      .post(this.userUrl + 'signup', { email, username, password })
      .pipe(catchError(this.handleError));
  }

  signin(usernameOrEmail: string, password: string): Observable<any> {
    return this.http
      .post(this.userUrl + 'signin', { usernameOrEmail, password })
      .pipe(catchError(this.handleError));
  }

  logout(): void {
    // TODO: instead delete cookie
    localStorage.removeItem('jwt');
  }

  isSignedIn(): boolean {
    // TODO
    return false;
  }
}
