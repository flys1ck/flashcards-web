import { ModuleWithProviders, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ErrorAlertComponent } from './components/error-alert/error-alert.component';
import { TranslateLoader, TranslateModule } from '@ngx-translate/core';
import { HttpClient } from '@angular/common/http';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';
import { NavigationComponent } from './components/navigation/navigation.component';
import { RouterModule } from '@angular/router';

export function createTranslateLoader(http: HttpClient): TranslateHttpLoader {
  return new TranslateHttpLoader(http, './assets/i18n/', '.json');
}
@NgModule({
  declarations: [ErrorAlertComponent, NavigationComponent],
  imports: [
    CommonModule,
    RouterModule,
    TranslateModule.forChild({
      loader: {
        provide: TranslateLoader,
        useFactory: createTranslateLoader,
        deps: [HttpClient],
      },
    }),
  ],
  exports: [TranslateModule, ErrorAlertComponent, NavigationComponent],
})
export class SharedModule {
  static forRoot(): ModuleWithProviders<TranslateModule> {
    return {
      ngModule: SharedModule,
      providers: [],
    };
  }
}
