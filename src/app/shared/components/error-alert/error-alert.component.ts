import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';

@Component({
  selector: 'app-error-alert[errorMessage]',
  templateUrl: './error-alert.component.html',
  styleUrls: ['./error-alert.component.css'],
})
export class ErrorAlertComponent {
  @Input() errorMessage?: string;
  @Output() closed = new EventEmitter();

  close(): void {
    this.closed.emit();
  }
}
