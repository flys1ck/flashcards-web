import { Component, OnInit } from '@angular/core';
import { UserService } from '@app/services/user.service';

@Component({
  selector: 'app-navigation',
  templateUrl: './navigation.component.html',
  styleUrls: ['./navigation.component.css'],
})
export class NavigationComponent implements OnInit {
  public isSignedIn: boolean;

  constructor(private userService: UserService) {
    this.isSignedIn = userService.isSignedIn();
  }

  ngOnInit(): void {}
}
